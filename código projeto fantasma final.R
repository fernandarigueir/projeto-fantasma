library(dplyr)
library(readxl)
library(tidyverse)
library(ggplot2)
library(labeling)
library(formattable)
library(data.table)

#Importando banco de dados
Dados_Dunder <- read.csv("DunderMifflinCompany.csv.csv", sep = ";")
Dados_Felicidade <- fread("Felicidade.csv")

#Padronizações da ESTAT
cores_estat <- c('#A11D21','#003366', '#CC9900', '#663333','#FF6600','#CC9966',
                 '#999966','#006606','#008091', '#041835','#666666')

theme_estat <- function(...) {
  theme <- ggplot2::theme_bw() +
    ggplot2::theme(
      axis.title.y = ggplot2::element_text(colour = "black", size = 12),
      axis.title.x = ggplot2::element_text(colour = "black", size = 12),
      axis.text = ggplot2::element_text(colour = "black", size = 9.5),
      panel.border = ggplot2::element_blank(),
      axis.line = ggplot2::element_line(colour = "black"),
      legend.position = "top",
      ...
    )
  
  return(
    list(
      theme,
      scale_fill_manual(values = cores_estat),
      scale_colour_manual(values = cores_estat)
    )
  )
}

#Retirando observações duplicadas do banco Dados_Dunder
dados <- Dados_Dunder[! duplicated (Dados_Dunder$Employee.ID),]

#Adicionando coluna com respectivos países.
dados<- dados %>%  
  mutate(País = case_when(grepl("Paulo", City) ~ "Brasil",
                          grepl("Rio", City) ~"Brasil",
                          grepl("Manaus", City) ~"Brasil",
                          grepl("Brasil", City) ~"Brasil",
                          grepl("Shanghai", City) ~ "China",
                          grepl("Chongqing", City) ~"China",
                          grepl("Chengdu", City) ~"China",
                          grepl("Beijing", City) ~"China",
                          grepl("Columbus", City) ~"Estados Unidos",
                          grepl("Chicago", City) ~"Estados Unidos",
                          grepl("Seattle", City) ~ "Estados Unidos",
                          grepl("Austin", City) ~"Estados Unidos",
                          grepl("Phoenix", City) ~"Estados Unidos",
                          grepl("Miami", City) ~"Estados Unidos"))

#Alterando a coluna data para ano de contratação 
dados$Hire.Date <- str_sub(dados$Hire.Date, start = -4)

#Renomeando para mostrar as contratações por ano e por país
glimpse(dados)
analise1 <- dados %>% 
  count(Hire.Date, País)

#Transformando a coluna de ano de contratação para valor numérico
analise1$Hire.Date <- as.numeric(analise1$Hire.Date)

#Plotando o gráfico da Análise 1 -----
ggplot(analise1) +
  aes(x = Hire.Date, y = n, group = País, colour = País) +
  geom_line(size = 1) +
  geom_point(size = 2) +
  scale_colour_manual(name = "Produto", labels = c("A", "B")) +
  labs(x = "Ano", y = "Número de Contratações") +
  scale_y_continuous(breaks = seq(0,100,by= 10))+
  scale_x_continuous(breaks = seq(1992,2022,by=3))+
  theme_estat()
ggsave("Analise1.pdf", width = 158, height = 93, units = "mm")

#Alterando a coluna de Salário anual para numérico para permitir extrair estatísticas.
dados$Annual.Salary <- str_sub(dados$Annual.Salary, start = 2)

dados$Annual.Salary <- str_replace(dados$Annual.Salary, ",",".")

dados$Annual.Salary <- as.numeric(dados$Annual.Salary)

#Traduzindo Unidades de negócio para o português
dados<- dados %>%  
  mutate(Business.Unit = case_when(grepl("Corporate", Business.Unit) ~ "Corporativo",
                                   grepl("Manufacturing", Business.Unit) ~"Manufaturado",
                                   grepl("Research", Business.Unit) ~"Pesquisa e Desenvolvimento",
                                   grepl("Products", Business.Unit) ~ "Produtos Especiais"))

#Plotando boxplot da Análise 2 ------
ggplot(dados) +
  aes(x = Business.Unit, y = Annual.Salary) +
  geom_boxplot(fill = c("#A11D21"), width = 0.5) +
  stat_summary(
    fun = "mean", geom = "point", shape = 23, size = 3, fill = "white"
  ) +
  labs(x = "Unidade de negócio", y = "Salário Anual (em milhares de dólares)") +
  scale_y_continuous(breaks = seq(0,300,by= 20))+
  scale_x_discrete(labels= c("Corporativo", "Manufaturado",
                             "Pesquisa e \nDesenvolvimento", "Produtos \nEspeciais"))+
  theme_estat()
ggsave("Analise2.pdf", width = 158, height = 93, units = "mm")

#Analisando estatísticas para tabela da Análise 2
summary(dados$Annual.Salary[dados$Business.Unit == "Corporativo"])
sd(dados$Annual.Salary[dados$Business.Unit == "Corporativo"])

summary(dados$Annual.Salary[dados$Business.Unit == "Manufaturado"])
sd(dados$Annual.Salary[dados$Business.Unit == "Manufaturado"])

summary(dados$Annual.Salary[dados$Business.Unit == "Pesquisa e Desenvolvimento"])
sd(dados$Annual.Salary[dados$Business.Unit == "Pesquisa e Desenvolvimento"])

summary(dados$Annual.Salary[dados$Business.Unit == "Produtos Especiais"])
sd(dados$Annual.Salary[dados$Business.Unit == "Produtos Especiais"])

#Plotando gráfico de barras da Análise 3 -----
analise3 <- dados %>%
  mutate(Business.Unit = case_when(
    Business.Unit %>% str_detect("Corporativo") ~ "Corporativo",
    Business.Unit %>% str_detect("Manufaturado") ~ "Manufaturado",
    Business.Unit %>% str_detect("Pesquisa") ~ "Pesquisa e Desenvolvimento",
    Business.Unit %>% str_detect("Produtos") ~ "Produtos Especiais"
    
  )) %>%
  group_by(Business.Unit, País) %>%
  summarise(freq = n()) %>%
  mutate(
    freq_relativa = round(freq*100/sum(freq,2),2)
  )

porcentagens <- str_c(analise3$freq_relativa, "%") %>% str_replace("\\.", ",")

legendas <- str_squish(str_c(analise3$freq, " (", porcentagens, ")"))

ggplot(analise3) +
  aes(
    x = fct_reorder(Business.Unit, freq, .desc = T), y = freq,
    fill = País, label = legendas
  ) +
  geom_col(position = position_dodge2(preserve = "single", padding = 0)) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, hjust = 0.5,
    size = 2.5
  ) +
  scale_y_continuous(limits=c(0, 200)) +
  labs(x = "Unidade de Negócio", y = "Frequência") +
  scale_x_discrete(labels= c("Corporativo", "Manufaturado",
                             "Pesquisa e \nDesenvolvimento", "Produtos \nEspeciais"))+
  theme_estat()
ggsave("Analise3.pdf", width = 158, height = 93, units = "mm")

#Arredondando números do Dados_Felicidade para duas casas decimais
Dados_Felicidade$Felicidade <- round(Dados_Felicidade$Felicidade,2)
Dados_Felicidade$`Satisfação com o trabalho` <- round(Dados_Felicidade$`Satisfação com o trabalho`,2)

#Plotando Gráfico de Dispersão da Análise 4 ----
ggplot(Dados_Felicidade) +
  aes(x = Felicidade, y = `Satisfa??o com o trabalho`) +
  geom_point(colour = "#A11D21", size = 3) +
  labs(
    x = "Felicidade",
    y = "Satisfação com o Trabalho"
  ) +
  scale_y_continuous(limits=c(0, 10)) +
  theme_estat()
ggsave("Analise4.pdf", width = 158, height = 93, units = "mm")

#Analisando estatísticas para tabela da Análise 4
summary(Dados_Felicidade$`Satisfação com o trabalho`)
sd(Dados_Felicidade$`Satisfação com o trabalho`)

summary(Dados_Felicidade$Felicidade)
sd(Dados_Felicidade$Felicidade)

#Traduzindo departamentos para o português
dados<- dados %>%  
  mutate(Department = case_when(grepl("Accounting", Department) ~ "Contabilidade",
                                grepl("Engineering", Department) ~"Engenharia",
                                grepl("Sales", Department) ~"Vendas",
                                grepl("Finance", Department) ~ "Finanças",
                                grepl("Human Resources", Department) ~ "Recursos Humanos",
                                grepl("Marketing", Department) ~ "Marketing",
                                grepl("IT", Department) ~ "Tecnologia da Informação"))

#Plotando Gráfico de Colunas da Análise 5 -----
analise5 <- dados %>%
  filter(!is.na(Department)) %>%
  count(Department) %>%
  mutate(
    freq = round(n*100/sum(n,2),2),
  ) %>%
  mutate(
    freq = gsub("\\.", ",", freq) %>% paste("%", sep = ""),
    label = str_c(n, " (", freq, ")") %>% str_squish()
  )

ggplot(analise5) +
  aes(x = fct_reorder(Department, n, .desc=T), y = n, label = label) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, hjust = .5,
    size = 3
  ) + 
  labs(x = "Departamento", y = "Frequência") +
  scale_y_continuous(limits=c(0, 300)) +
  scale_x_discrete(labels= c("Tecnologia da \nInformação", "Vendas",
                             "Engenharia", "Contabilidade", "Marketing",
                             "Recursos \nHumanos", "Finanças"))+
  theme_estat()
ggsave("Analise5.pdf", width = 158, height = 93, units = "mm")

#Plotando gráfico de colunas para Análise 6 ----
analise6 <- dados %>%
  filter(!is.na(City)) %>%
  count(City) %>%
  mutate(
    freq = round(n*100/sum(n,2),2),
  ) %>%
  mutate(
    freq = gsub("\\.", ",", freq) %>% paste("%", sep = ""),
    label = str_c(n, " (", freq, ")") %>% str_squish()
  )

analise6 <- top_n(analise6,5,n)

ggplot(analise6) +
  aes(x = fct_reorder(City, n, .desc=T), y = n, label = label) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .9),
    vjust =-0.3, hjust = 0.5,
    size = 3
  ) +
  labs(x = "Departamento", y = "Frequência") +
  scale_y_continuous(limits=c(0, 150)) +
  theme_estat()
ggsave("Analise6.pdf", width = 158, height = 93, units = "mm")

